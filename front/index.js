//Aqui ele acessa a URL
const fetchPromise = 
fetch("http://www.mocky.io/v2/5edbc75532000008a05d27f5/autores");

//Aqui ele vai pegar o id=main, que está em um dos div do html
const main = document.getElementById("main");

//Aqui é pra ele ler o json da URL
fetchPromise.then(response => {
  return response.json();

//Aqui é pra ele ententer qual informação buscar
}).then(people => {
  main.innerHTML = listOfNames(people);
});

//Aqui especifica que queremos apenas o nome, e ele manda o que achou para o html
function listOfNames(people) {
  const nomes = people.map(person => `<li>${person.apelido}</li>`).join("\n");
  return `<ul>${nomes}</ul>`
}