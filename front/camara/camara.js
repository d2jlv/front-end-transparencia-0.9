//variável que vai carregar as informações do endpoint da api
var info = []


//funcao xmlhttprequest que vai pegar as info do endpoint e jogar na variavel info
function PegaInfo(){
    
    var xmlhttp = new XMLHttpRequest();
    var url = "http://www.mocky.io/v2/5edbc75532000008a05d27f5/autores";
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            for(var i =0; i<myArr.length; i++){
                info.push(myArr[i]);
            }
            var nomes = pegaApelido(info);
            var ids = pegaId(info);
            makeSelect(nomes);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}
PegaInfo();

function pegaApelido(info) {
    var out = [];
    for(var i = 0; i < info.length; i++) {
        out[i] = info[i].apelido ;
    }
    console.log(out);
    return out;
}

function pegaId(info) {
    var out = "";
    for(var i = 0; i < info.length; i++) {
        out = out + info[i].id;
    }
    return out;
}

function makeSelect(nomes){;
    var select = document.getElementById("vereadores");
    for (var i = 0; i < info.length; i++){
      var lst = nomes[i];
      var opt = document.createElement('option');
      opt.textContent = lst;
      opt.value = lst;
      
      select.add(opt);
    }
}

function optionselect(){
    var d=document.getElementById("vereadores").value;
    var xmlhttp = new XMLHttpRequest();
    var url = "http://www.mocky.io/v2/5edbc75532000008a05d27f5/autores";
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            for(var i =0; i<myArr.length; i++){
                info.push(myArr[i]);
            }
            idEscolhido = comparaId(info, d);
            mandaDetalhe(idEscolhido);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function comparaId(info, d){
    var id;
    for (var i = 0; i < info.length; i++){
        if(d === info[i].apelido){
            id = info[i].id;
        }
    }
    return id;
}

function mandaDetalhe(id){
    var xmlhttp = new XMLHttpRequest();
    var url = "http://www.mocky.io/v2/5edbdbc63200001d9f5d281b/autores/" + id;
    console.log(url);
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            console.log(myArr);
            document.getElementById("botao").onclick = function() {detalheTela(myArr)};
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function detalheTela(arr){
    


    for (var i = 0; i < arr.length; i++){
    var detalhe = 
        '<p> Informações sobre o candidato escolhido: </p>' +
        '<p>     Nome: ' + arr[i].nome + '</p>'+
        '<p>     Partido: ' + arr[i].partido + '</p>'+
        '<p>     Atividade: ' + arr[i].atividade + '</p>'+
        '<p>     Formação Acadêmica: ' + arr[i].formacao + '</p>'+
        '<p>     Data de Nascimento: ' + arr[i].dataNascimento + '</p>'+
        '<p>     Estado Civil: ' + arr[i].estadoCivil + '</p>'+
        '<p>     Quantidade de Votos Recebidos: ' + arr[i].votosRecebidos + '</p>'+
        '<p>     Legistatura: ' + arr[i].legislatura + '</p>' +
        '<p>     Quantidade de Projetos: ' + arr[i].qnteProjetos + '</p>' +
        '<p><br></p>' +
        '<p>Projetos:</p>' +
        '<p><br></p>' 
        for(var j=0; j<arr[i].projetos.length; j++){
            detalhe = detalhe +
            '<p>     Número do Projeto: ' + arr[i].projetos[j].numero + '</p>'+
            '<p>     Assunto: ' + arr[i].projetos[j].assunto + '</p>'+
            '<p>     Status: ' + arr[i].projetos[j].status + '</p>'+
            '<p>     Ano: ' + arr[i].projetos[j].ano + '</p>'+
            '<p>     Link para o Projeto: ' + arr[i].projetos[j].link + '</p>'
        }
    }
    document.getElementById("main").innerHTML = detalhe;
    
}



